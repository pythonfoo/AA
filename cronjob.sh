#!/usr/bin/env bash


# erstes Argument: Pfad zum Ordner, in dem manage.py liegt.

# zB /home/$user/mvw/AA/ROOT
cd $1

# Pythonumgebung mit allen installierten Abhängigkeiten aktivieren
source ../env/bin/activate

# Alle regelmäßigen Aufgaben der Mitgliederverwaltung ausführen
python manage.py scheduler