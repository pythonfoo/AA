#!/usr/bin/env bash

set -e

source env/bin/activate

# Make a backup of the database
pg_dump -Fc AA > ROOT/postgresql.db.backup_$(date '+%Y-%m-%d_%H-%M-%S').dump


# Get newest released version
git checkout main
git pull

# Install exactly all needed dependencies
pip install --upgrade -r requirements.txt

# Make sure all database migrations are applied
python ROOT/manage.py migrate

# Make sure all static files are in place
python ROOT/manage.py collectstatic --noinput

# Test if gpg is working in a subshell
(cd ROOT && python -Wall manage.py test members.test_gpg.GpgTestCase)

# Start webserver
gunicorn ROOT.wsgi --timeout 14400 --chdir ROOT/ --bind :8000
