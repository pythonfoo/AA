import gpg
from other import gpg_util
from django.conf import settings
from django.test import TestCase, override_settings

SKS_KEYSERVER = "hkp://pgpkeys.eu"
OPENPGP_KEYSERVER = "hkps://keys.openpgp.org"
UNREACHABLE_KEYSERVER = "hkps://keys.example.com"

EXPIRED_KEYID = "0x53b620d01ce0c630"  # GNUPG Signing Key until 2010
VALID_KEYID = "0xacc2602f3f48cb21"  # Qubes OS Security
VALID_FPR = "B35B2DA4B9F9F10949226F77ACC2602F3F48CB21" # Qubes OS Security
REVOKED_KEYID = "0xF8663DD303E12B58"  # Old, revoked key of a Linux kernel developer
UNRETRIEVABLE_KEYID = "0x1111111111111111"  # Let's assume nobody finds this as a vanity ID
OPENPGPONLY_KEYID = "0x4D66CB0FEBA5DAF1"  # security@sequoia-pgp.org ... probably won't start using SKS
EVIL32_KEYID = "0x10000001"
INVALID_KEYID = "AINTAKEYID"
SE_KEYID = "4B6B9174638813A83E8368ED059ACC5D37B1245E"  # Part of the test fixture keyring
NOUID_KEYID = "3AF8806E21F3BBD8B617AD99917424F6FE7059A1"  # Has no UIDs
WKD_ADVANCED_UID = ("0x4D66CB0FEBA5DAF1", "security@sequoia-pgp.org")
WKD_DIRECT_UID = ("0xDB24801DB9EFECF5", "webmaster@kuketz-blog.de")  # Kuketz uses the direct method without subdomain
WKD_NOSERV_UID = ("0x1111111111111111", "doesnotexist@example.org")  # WKD server for domain does not exist
WKD_NOKEY_UID = ("0x1111111111111111", "doesnotexist@sequoia-pgp.org")  # WKD server exists, but user does not
WKD_MISMATCH_UID = ("0x1111111111111111", "security@sequoia-pgp.org")  # Key for uid exists, but doesn't match keyid
TEST_MSG = 'tästè'

ALL_KEYIDS = [
    EXPIRED_KEYID, VALID_KEYID, REVOKED_KEYID, UNRETRIEVABLE_KEYID,
    OPENPGPONLY_KEYID, EVIL32_KEYID, INVALID_KEYID
]

ALL_KEYSERVERS = [SKS_KEYSERVER, OPENPGP_KEYSERVER, UNREACHABLE_KEYSERVER]


def decrypt(ciphertext):
    with gpg.Context(armor=True) as ctx:
        ctx.set_engine_info(gpg.constants.protocol.OpenPGP,
                            home_dir=settings.GPG_HOME)
        result = ctx.verify(ciphertext)
        result = ctx.decrypt(ciphertext)
        return result[0]


def verify(signed_msg):
    with gpg.Context(armor=True) as ctx:
        ctx.set_engine_info(gpg.constants.protocol.OpenPGP,
                            home_dir=settings.GPG_HOME)
        result = ctx.verify(signed_msg)
        return result[0]


class GpgTestCase(TestCase):
    def setUp(self):
        ctx = gpg.Context(armor=True)
        ctx.set_engine_info(gpg.constants.protocol.OpenPGP,
                            home_dir=settings.GPG_HOME)
        ctx.set_keylist_mode(gpg.constants.keylist.mode.LOCAL)
        for keyid in ALL_KEYIDS:
            try:
                key = ctx.get_key(keyid)
                gpg._gpgme.gpgme_op_delete(ctx.wrapped, key,
                                           gpg._gpgme.GPGME_DELETE_FORCE)
            except Exception:
                # Errors don't matter, we just need a clean keyring
                # Most of the keys will not even be in the keyring at setup
                pass

    # Test some basic assumptions for more complex testcases first.
    # Since the keyservers and keys are not under our control, these
    # examples may start failing!
    def test_basic_retrieval(self):
        for search_term, keyserver in [
            (VALID_KEYID, SKS_KEYSERVER),
            (VALID_KEYID, OPENPGP_KEYSERVER),
            (VALID_FPR, SKS_KEYSERVER),
            (VALID_FPR, OPENPGP_KEYSERVER),
        ]:
            with self.subTest(search_term=search_term, keyserver=keyserver):
                src = gpg_util.HkpKeySource(keyserver)
                key = src._retrieve_key(search_term)
                self.assertIsNotNone(key)
                self.assertEqual(key.keyid.lower()[-16:], search_term.lower()[-16:])

    def test_unretrievable_is_unretrievable(self):
        src = gpg_util.HkpKeySource(SKS_KEYSERVER)
        key = src._retrieve_key(UNRETRIEVABLE_KEYID)
        self.assertEqual(key, None)

        src = gpg_util.HkpKeySource(OPENPGP_KEYSERVER)
        key = src._retrieve_key(UNRETRIEVABLE_KEYID)
        self.assertEqual(key, None)

    def test_openpgp_exclusive(self):
        src = gpg_util.HkpKeySource(SKS_KEYSERVER)
        key = src._retrieve_key(OPENPGPONLY_KEYID)
        self.assertEqual(key, None)

        src = gpg_util.HkpKeySource(OPENPGP_KEYSERVER)
        key = src._retrieve_key(OPENPGPONLY_KEYID)
        self.assertEqual(key.keyid.lower()[-16:],
                         OPENPGPONLY_KEYID.lower()[-16:])

    def test_revoked_is_revoked(self):
        src = gpg_util.HkpKeySource(SKS_KEYSERVER)
        key = src._retrieve_key(REVOKED_KEYID)
        self.assertEqual(key.keyid.lower()[-16:], REVOKED_KEYID.lower()[-16:])
        self.assertNotEqual(key.identities, [])
        # Plausibility Check - the key and revocation is only served without identity on openpgp.org
        src = gpg_util.HkpKeySource(OPENPGP_KEYSERVER)
        key = src._retrieve_key(REVOKED_KEYID)
        self.assertEqual(key.identities, [])

    def test_unreachable_keyserver(self):
        src = gpg_util.HkpKeySource(UNREACHABLE_KEYSERVER)
        with self.assertRaises(gpg_util.ServerUnavailableError):
            src._retrieve_key(VALID_KEYID)

    def test_ambiguous_keyid(self):
        with self.assertRaises(gpg_util.AmbiguousKeyIDError):
            src = gpg_util.HkpKeySource(SKS_KEYSERVER)
            src._retrieve_key(EVIL32_KEYID)
        with self.assertRaises(gpg_util.InvalidRequestError):
            # Presumably, only short KeyIDs can be ambiguous
            # Hagrid refuses them outright
            src = gpg_util.HkpKeySource(OPENPGP_KEYSERVER)
            src._retrieve_key(EVIL32_KEYID)

    def test_wkd_preflight(self):
        src = gpg_util.WkdKeySource()
        src.retrieve_key(*WKD_ADVANCED_UID)
        src.retrieve_key(*WKD_DIRECT_UID)
        src.retrieve_key(*WKD_MISMATCH_UID)
        # Actual exception depends on return code (e.g. 404 vs 500 or NXDOMAIN)
        with self.assertRaises((gpg_util.InvalidRequestError, gpg_util.ServerUnavailableError)):
            key = src.retrieve_key(*WKD_NOSERV_UID)
        with self.assertRaises(gpg_util.InvalidRequestError):
            key = src.retrieve_key(*WKD_NOKEY_UID)

    @override_settings(GPG_WKD=True, GPG_KEYSERVERS=[])
    def test_wkd_advanced_retrieval(self):
        key = gpg_util.PublicKey(*WKD_ADVANCED_UID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.AVLBL)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.CURRENT)
        self.assertTrue(key.key.can_encrypt)

    @override_settings(GPG_WKD=True, GPG_KEYSERVERS=[])
    def test_wkd_direct_retrieval(self):
        key = gpg_util.PublicKey(*WKD_DIRECT_UID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.AVLBL)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.CURRENT)
        self.assertTrue(key.key.can_encrypt)

    @override_settings(GPG_WKD=True, GPG_KEYSERVERS=[])
    def test_wkd_keyid_mismatch(self):
        key = gpg_util.PublicKey(*WKD_MISMATCH_UID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)

    @override_settings(GPG_WKD=True, GPG_KEYSERVERS=[])
    def test_wkd_noserver(self):
        key = gpg_util.PublicKey(*WKD_NOSERV_UID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)

    @override_settings(GPG_WKD=True, GPG_KEYSERVERS=[])
    def test_wkd_nokey(self):
        key = gpg_util.PublicKey(*WKD_NOKEY_UID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)

    # Testcases for Key Status Management
    def test_key_initialization(self):
        key = gpg_util.PublicKey(VALID_KEYID)
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.NA)

    def test_key_refresh(self):
        key = gpg_util.PublicKey(VALID_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.AVLBL)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.CURRENT)
        key2 = gpg_util.PublicKey(VALID_KEYID)
        self.assertEqual(key2.keystatus, gpg_util.KeyStatus.AVLBL)
        self.assertEqual(key2.refresh_status, gpg_util.KeyRefreshStatus.STORED)

    @override_settings(GPG_KEYSERVERS=[UNREACHABLE_KEYSERVER])
    def test_unreachable_status(self):
        key = gpg_util.PublicKey(VALID_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.SRVERR)

    @override_settings(GPG_KEYSERVERS=[SKS_KEYSERVER, UNREACHABLE_KEYSERVER])
    def test_one_reachable_server_suffices(self):
        key = gpg_util.PublicKey(VALID_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.AVLBL)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.CURRENT)

    def test_key_revoked(self):
        key = gpg_util.PublicKey(REVOKED_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.REVOKED)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.CURRENT)

    def test_key_expired(self):
        key = gpg_util.PublicKey(EXPIRED_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.EXPIRED)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.CURRENT)

    def test_key_invalid(self):
        key = gpg_util.PublicKey(INVALID_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.INVALID)

    def test_key_ambiguous(self):
        key = gpg_util.PublicKey(EVIL32_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)
        self.assertEqual(key.refresh_status,
                         gpg_util.KeyRefreshStatus.AMBIGUOUS)

    def test_key_no_UID(self):
        key = gpg_util.PublicKey(NOUID_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.NA)
        self.assertEqual(key.refresh_status,
                         gpg_util.KeyRefreshStatus.IMPORTERR)

    def test_signature(self):
        signing_result = gpg_util.sign_message(TEST_MSG, SE_KEYID)
        result = verify(signing_result[0])
        returned_text = result.decode('utf-8')
        # Clearsigning adds a newline ... is that documented anywhere?
        self.assertEqual(TEST_MSG + '\n', returned_text)

    def test_encryption(self):
        key = gpg_util.PublicKey(SE_KEYID)
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.AVLBL)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.STORED)
        ciphertext, _, _ = key.encrypt_to(TEST_MSG, SE_KEYID)
        plaintext = decrypt(ciphertext).decode('utf-8')
        self.assertEqual(TEST_MSG, plaintext)

    def test_encryption_with_imported_key(self):
        key = gpg_util.PublicKey(VALID_KEYID)
        key.refresh()
        self.assertEqual(key.keystatus, gpg_util.KeyStatus.AVLBL)
        self.assertEqual(key.refresh_status, gpg_util.KeyRefreshStatus.CURRENT)
        ciphertext, _, _ = key.encrypt_to(TEST_MSG, SE_KEYID)
