import csv
import io
import os
from datetime import date, datetime
from wsgiref.util import FileWrapper

from django import forms
from django.conf import settings
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import HttpResponse, redirect, render, get_object_or_404
from django.http import JsonResponse
from django.db.models import Min
from members.countryfield import get_country_dict
from members.forms_members import MemberImportForm
from members.forms_revert_transaction import BalanceTransactionLogForm
from members.forms_search import SearchForm
from members.models import BalanceTransactionLog, EmailAddress, Erfa, Member, Subscriber, ArchivedEmail, EmailToMember, \
    _initial_password, _unique_username, AbstimmungToken
from members.forms_ds_pa_select import DSAddressLabelForm
from members.forms_pa_select import AddressLabelForm
from members.forms_validate_teilnahmetoken import TeilnahmeValidationForm
from ROOT.settings import EMPTY_ERFA_NAME
from import_app.models import Transaction
from api.views import datenschleuder_address_stickers, ga_invitations

from .api_helper import Vereinstisch, Openslides, cashpoint_export_impl, premiumaddress_reader, ErfaCSVExporter
from .forms_erfa import (
    ErfaSelectFileUploadForm, ErfaSelectFileUploadFormAll, ErfaSelectForm,
    ErfaSelectFormAll, SimpleFileUploadForm,
)


def add_member(request):
    from .forms_members import MemberAddForm, EmailFormset

    new_chaos_number = 0
    if request.method == 'POST':
        member_form = MemberAddForm(prefix='member', data=request.POST)
        email_formset = EmailFormset(data=request.POST)
        if member_form.is_valid():
            new_member = member_form.save(commit=False)
            # new_member.chaos_number = -1
            email_formset = EmailFormset(data=request.POST, instance=new_member)
            if email_formset.is_valid():
                # new_member.chaos_number = None
                # Set the following dates to the same values as otherwise the logic for requesting the first
                # payment in the welcome mail gets confused
                new_member.save()
                email_formset.save()
                new_chaos_number = new_member.chaos_number

                # prepare new empty member form
                email_formset = EmailFormset()
                member_form = MemberAddForm(prefix='member')
    else:
        email_formset = EmailFormset()
        member_form = MemberAddForm(prefix='member')
    return render(request, 'add_member.html', {'member_form': member_form,
                                               'email_formset': email_formset,
                                               'new_chaos_number': new_chaos_number})


def change_membership_type(request, cnr=None):
    """
    Changes membership type from supporter to member (with new chaos number)
    :param request: GET request with parameter 'chaos_number'
    :return: result as json
    """
    if not cnr:
        return JsonResponse({'result': 'parameter chaos_number missing'})

    # m is the member with CNR
    try:
        m = Member.objects.get(chaos_number=cnr)
    except Member.DoesNotExist:
        return JsonResponse({'result': 'member with provided chaos number does not exist'})

    if not m.is_member():
        return JsonResponse({'result': 'member exited already'})

    if m.membership_type == Member.MEMBERSHIP_TYPE_MEMBER:
        return JsonResponse({'result': 'membership type is already member'})

    # Wenn wir die CNR und pk (primary key) auf None setzen, kriegt das Mitglied automatisch eine neue bei save()
    m.chaos_number = None
    m.pk = None
    m.username = _unique_username()
    m.initial_password = _initial_password()

    # m hat noch den falschen Mitgliedschaftsbeginn.
    m.membership_start = date.today()
    m.membership_type = Member.MEMBERSHIP_TYPE_MEMBER
    m.save()

    # o wie Original.
    o = Member.objects.get(chaos_number=cnr)
    o.membership_end = date.today()
    o.save()

    # so bekommt m die Emailadressen von o:
    for email in o.emailaddress_set.all():
        email.pk=None
        email.person=m
        email.save()

    # m bekommt noch ein Kommentar, dass es importiert wurde:
    m.comment = ("" if not m.comment else m.comment + " ") + "Importiert von Fördermitglied Nr. " + str(o.chaos_number)
    m.save()
    return JsonResponse({'result': 'success'})


def _combine_values(all_fields_ar):
    final_string = ' '.join(all_fields_ar)
    return final_string.strip()


def _bool_converter(value):
    value = value.lower()
    if value in ['true', 'wahr', 'war']:
        return True
    elif value in ['false', 'falsch']:
        return False
    else:
        print('WARNING: VALUE IS NOT BOOL: "' + value + '"')
        return False


def _date_re_format(bad_date):
    if bad_date == '':
        return None
    try:
        return datetime.strptime(bad_date, '%d.%m.%y').date()
    except ValueError:
        print('WARNING: BAD DATE IS STRANGE: "' + bad_date + '"')
        return None


_erfaWithDoppelmitgliedschaft = ['HHV', 'ACV', 'C4', 'ZHV', 'PBV', 'DUV', 'XMZ', 'MAV', 'CKL']


_erfaLongName = {
    'HH': 'Hamburg',
    'B': 'Berlin',
    'K': 'Köln',
    'ULM': 'Ulm',
    'DUE': 'Düsseldorf',
    'KAR': 'Karlsruhe',
    'HAN': 'Hannover',
    'MUC': 'München',
    'ERL': 'Erlangen',
    'KAS': 'Kassel',
    'AC': 'Aachen',
    'W': 'Wien',
    'DA': 'Darmstadt',
    'DD': 'Dresden',
    'ZUE': 'Zürich',
    'MZ': 'Mainz',
    'PB': 'Paderborn',
    'HB': 'Bremen',
    'MA': 'Mannheim',
    'FFM': 'Frankfurt a.M.',
    'GOE': 'Göttingen',
    'FR': 'Freiburg',
    'E': 'Essen',
    'S': 'Stuttgart',
    'KL': 'Kaiserslautern',
    'BA': 'Bamberg',

    # Vereine ohne Erfa-Rueckerstattung
    'HHV': 'Hamburg e.V.',
    'ACV': 'Aachen e.V.',
    'C4': 'Köln C4 e.V.',
    'ZHV': 'Zürich Verein',
    'PBV': 'Paderborn e.V.',
    'DUV': 'Düsseldorf e.V.',
    'XMZ': 'Mainz e.V.',
    'MAV': 'Mannheim e.V.',
    'CKL': 'Kaiserslautern e.V.'
}

_erfaFixDict = {
    'HAM': 'HH',
    'BLN': 'B',
    'EL': 'ERL',
    'KLN': 'K',
    'UML': 'ULM',
    'UL': 'ULM',
    'D': 'DUE',
    'DUS': 'DUE',
    'DU': 'DUE',
    'FRA': 'FFM',
    'F': 'FFM',
    'KA': 'KAR',
    'H': 'HAN',
    'M': 'MUC',
    'KS': 'KAS',
    'AA': 'AC',
    'A': 'AC',
    'AAC': 'AC',
    'WIE': 'W',
    'DRE': 'DD',
    'C3D': 'DD',
    'Z': 'ZUE',
    'ZH': 'ZUE',
    'BRE': 'HB',
    'FRE': 'FR',
    'CZH': 'ZHV',
    'CHZ': 'ZHV',
    'BR': 'HB',
    'ST': 'S',
    'AB': 'BA',
    'ER': 'ERL',
    'HL': EMPTY_ERFA_NAME,
    'BIE': EMPTY_ERFA_NAME,
    'CH': EMPTY_ERFA_NAME,
    'ALI': EMPTY_ERFA_NAME,
    'MG': EMPTY_ERFA_NAME,
    'L': EMPTY_ERFA_NAME,
    'OS': EMPTY_ERFA_NAME,
    'N': EMPTY_ERFA_NAME,
    'RUH': EMPTY_ERFA_NAME,
    'BO': EMPTY_ERFA_NAME,
    'TR': EMPTY_ERFA_NAME,
    'RP': EMPTY_ERFA_NAME,
    'HEI': EMPTY_ERFA_NAME,
    'MUE': EMPTY_ERFA_NAME,
    'BAS': EMPTY_ERFA_NAME,
    'DO': EMPTY_ERFA_NAME,
    'MS': EMPTY_ERFA_NAME,
    'R': EMPTY_ERFA_NAME,
    'MRM': EMPTY_ERFA_NAME,
    'GIE': EMPTY_ERFA_NAME,
    'HEF': EMPTY_ERFA_NAME,
    'BSL': EMPTY_ERFA_NAME,
    'HD': EMPTY_ERFA_NAME,
    '': EMPTY_ERFA_NAME
}

_countryDict = get_country_dict()
_countryFixDict = {
    'D 6': 'DE', 'D': 'DE', 'DEU': 'DE', 'DD': 'DE',
    'UK': 'GB',
    'US9': 'US', 'USA': 'US',
    'C': 'CH',
    'CHI': 'CN',
    'ITA': 'IT', 'I': 'IT',
    'JOR': 'JO',
    'SGP': 'SG',
    'ARG': 'AR',
    'NOR': 'NO',
    'F': 'FR',
    'IRL': 'IE',
    'AUS': 'AU',
    'N': 'NO',
    'RUS': 'RU',
    'A': 'AT', 'N': 'AT',
    'H': 'HU',
    'B': 'BE',
    'FIN': 'FI',
    'IND': 'ID',
    'E': 'ES',
    'L': 'LU',
    'P': 'PL'
}


def _country_fix(country):
    country = country.upper()
    if country == '':
        return ''
    elif country in _countryDict:
        return country
    elif country in _countryFixDict:
        return _countryFixDict[country]
    else:
        print('WARNING: BAD COUNTRY: "' + country + '"')
        return country


def run_import_members(request):
    def is_datenschleuder(row):
        return (row['RST,N,3,0'].isnumeric() and int(row['RST,N,3,0']) < 999) \
            or _bool_converter(row['PRV,L']) or _bool_converter(row['AAA,L']) or _bool_converter(row['FRE,L'])

    def add_ds_member(row):
        # datenschleuder MTG,L, False / True <- Darauf kann man sich nicht verlassen
        # row['RST,N,3,0' # Datenschleuder max-issue
        new_datenschleuder_subscriber = Subscriber()

        new_datenschleuder_subscriber.chaos_number = int(row['NR,N,5,0'])

        new_datenschleuder_subscriber.subscriber_source = Subscriber.IMPORT_INITIAL

        new_datenschleuder_subscriber.first_name = row['VNM,C,10']
        new_datenschleuder_subscriber.last_name = row['NME,C,20']

        new_datenschleuder_subscriber.address_1 = _combine_values([
            row['ORG,C,30'],
            row['STR,C,30'],
        ])
        new_datenschleuder_subscriber.address_2 = _combine_values([
            row['PLZ,N,5,0'],
            row['ORT,C,40'],
        ])
        new_datenschleuder_subscriber.address_3 = ''
        new_datenschleuder_subscriber.address_country = _country_fix(row['LND,C,3'])

        if (not new_datenschleuder_subscriber.address_1.strip() and
                not new_datenschleuder_subscriber.address_2.strip()) or _bool_converter(row['UBK,L']):
            new_datenschleuder_subscriber.address_unknown = settings.ADDRESS_RETURNS

        if not _bool_converter(row['AKT,L']):
            new_datenschleuder_subscriber.max_datenschleuder_issue = 0
        else:
            new_datenschleuder_subscriber.max_datenschleuder_issue = int(row['RST,N,3,0'])
        new_datenschleuder_subscriber.is_endless =\
            _bool_converter(row['PRV,L']) or _bool_converter(row['AAA,L']) or _bool_converter(row['FRE,L'])

        # Fehler in dem Skript zum Eintragen der DS-Abos hat den : durch ein ; ersetzt
        if row['KOM,C,60'].startswith('https;'):
            new_datenschleuder_subscriber.rt_link = row['KOM,C,60'].replace(';', ':')

        new_datenschleuder_subscriber.save()
        return new_datenschleuder_subscriber

    def import_add_member(line_num, row):
        new_member = Member()
        # This instance only attribute is being evaluated by the save signal and ensures to not send a welcome email
        new_member._imported = True

        new_member.chaos_number = row['NR,N,5,0']

        if _bool_converter(row['EZE,L']):
            new_member.membership_type = Member.MEMBERSHIP_TYPE_SUPPORTER
        else:
            new_member.membership_type = Member.MEMBERSHIP_TYPE_MEMBER

        new_member.membership_reduced = _bool_converter(row['ERM,L'])

        new_member.first_name = row['VNM,C,10']
        new_member.last_name = row['NME,C,20']

        '''
        ORG,C,30
        Organisation (Teil der Anschrift), 30 Zeichen

        STR,C,30
        Straße

        PLZ,N,5,0
        Postleitzahl, 5-stellige Zahl, per Default 0

        ORT,C,40

        BND,C,30d
        Bundesland, eigentlich ungenutzt, steht aber in Einträgen <= 2043 oder
        so was drin.
        '''
        new_member.address_1 = _combine_values([
            row['ORG,C,30'],
            row['STR,C,30'],
        ])
        new_member.address_2 = _combine_values([
            row['PLZ,N,5,0'],
            row['ORT,C,40'],
        ])
        new_member.address_3 = ''

        if (not new_member.address_1.strip() and not new_member.address_2.strip()) or _bool_converter(row['UBK,L']):
            new_member.address_unknown = settings.ADDRESS_RETURNS

        '''
        LND,C,3
        Land
        '''
        new_member.address_country = _country_fix(row['LND,C,3'])

        new_member.comment = row['KOM,C,60']

        erfa_str = row['ERK,C,3'].upper().strip()

        if erfa_str in _erfaFixDict:
            erfa_str = _erfaFixDict[erfa_str]

        try:
            erfa = Erfa.objects.get(short_name=erfa_str)
        except Erfa.DoesNotExist:
            print('New ERFA: "' + erfa_str + '"')
            erfa = Erfa()
            erfa.short_name = erfa_str
            if erfa_str in _erfaLongName:
                erfa.long_name = _erfaLongName[erfa_str]
            else:
                erfa.long_name = erfa_str
            erfa.has_doppelmitgliedschaft = erfa_str in _erfaWithDoppelmitgliedschaft
            erfa.save()

        if erfa is not None:
            new_member.erfa = erfa

        '''
        LZA,D
        Letzte Zahlung am, Datum

        EIN,D
        Eintrittsdatum

        BZB,D
        Bezahlt bis

        AAM,D
        Ausgetreten am
        '''

        new_member.last_update = _date_re_format(row['LUP,D'])

        membership_start = _date_re_format(row['EIN,D'])
        if membership_start is None:
            membership_start = datetime.strptime('12.09.1981', '%d.%m.%Y').date()  # datetime.date(1981, 12, 9)#
        new_member.membership_start = membership_start

        last_paid = _date_re_format(row['LZA,D'])
        if last_paid:
            new_member.fee_last_paid = last_paid

        paid_until = _date_re_format(row['BZB,D'])
        if paid_until is None:
            new_member.fee_paid_until = membership_start
        else:
            new_member.fee_paid_until = paid_until

        new_member.membership_end = _date_re_format(row['AAM,D'])
        # temporarily inactive member
        if new_member.membership_end is None and not _bool_converter(row['AKT,L']):
            new_member.is_active = False

        new_member.save()

        email_str = row['EML,C,50'].strip()
        if email_str != '':
            emails_ar = email_str.split(',')
            first_mail = True
            for mailStr in emails_ar:
                mail = EmailAddress()
                mail.person = new_member
                mail.email_address = mailStr.strip().lower()
                mail.gpg_key_id = row['PGPKEY,C,42'].strip()
                mail.is_primary = first_mail
                first_mail = False
                try:
                    # This instance only attribute is being evaluated by the save signal and ensures to not send a
                    # welcome email
                    mail._imported = True
                    mail.save()
                except Exception as exception:
                    errors.append('Error at line ' + str(line_num) + ' @ mail "' + mailStr + '" Message: ' +
                                  str(exception) + ' LINE: ' + str(row))

        # exited member, delete all data
        if new_member.membership_end is not None or 'AUTO: geloescht wg. Zahlungsmangel' in new_member.comment:
            new_member.exit()

        return new_member

    # Handle file upload
    uploaded_file = ''
    errors = []
    imported = []
    if request.method == 'POST':
        form = MemberImportForm(request.POST, request.FILES)
        if form.is_valid():

            uploaded_file = request.FILES['docfile']
            data = uploaded_file.read().decode("latin-1")  # windows-1252

            f = io.StringIO(data)
            csv_data = csv.DictReader(f, delimiter="\t")

            for linenum, csv_row in enumerate(csv_data):
                try:
                    if is_datenschleuder(csv_row):
                        imported.append(str(add_ds_member(csv_row)))
                    else:
                        imported.append(str(import_add_member(linenum, csv_row)))
                except Exception as ex:
                    errors.append(
                        'Error at line ' + str(linenum) +
                        ' Message: ' + str(ex) +
                        ' LINE: "' + str(csv_row) + '"')

    form = MemberImportForm()  # An empty, unbound form
    response = render(
        request, 'import_members.html',
        {'form': form, 'uploaded_file': uploaded_file, 'errors': errors, 'imported': imported},
    )
    return response


def search_member_db(request):
    # TODO: if request.method == 'GET':
    # Show complete page including search
    # TODO: if request.method == 'POST':
    # Redirect to search API
    if request.method == 'POST':
        return HttpResponse("OK")
    else:
        form = SearchForm()
        return render(request, 'search_form.html', {'form': form})
    pass


def monthly_statistics(request):
    context = {'statistics_type': 'monthly',
               'month': date.today().month
               }
    return render(request, 'statistics.html', context)


def zip_statistics(request):
    return render(request, 'statistics_zip.html')


def general_statistics(request):
    # Put Zip analysis, country analysis a.s.o. here
    pass


def cashpoint_export(request):
    if request.method != 'POST':
        return render(request, 'erfaabgleich_export.html',
                      {'form': '', 'h1': 'Cash Point export'})

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=cashpoint.csv'
    cashpoint_export_impl(response, Member.objects.all())
    return response


def vereinstisch_export(request):
    if request.method == "POST":
        csv_export = Vereinstisch(Member.objects.all().order_by('last_name', 'first_name'))
        f = io.StringIO()
        csv_export.do_export(f)
        response = HttpResponse(f.getvalue(), content_type='application/text')
        response['Content-Disposition'] = 'attachment; filename=vereinstisch.csv'
        return response
    else:
        form = ''
    return render(request, 'erfaabgleich_export.html', {'form': form, 'h1': 'Vereinstisch export'})


def openslides_export(request):
    if request.method == "POST":
        csv_export = Openslides()
        f = io.StringIO()
        csv_export.do_export(f)
        response = HttpResponse(f.getvalue(), content_type='application/text')
        response['Content-Disposition'] = 'attachment; filename=openslides.csv'
        return response
    else:
        form = ''
    return render(request, 'erfaabgleich_export.html', {'form': form, 'h1': 'Openslides export'})


def vereinstisch_import(request):
    import_logs = None
    warnings = None
    form_class = SimpleFileUploadForm
    if request.method == 'POST':
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            uploaded_file = request.FILES['docfile']
            data = uploaded_file.read().decode('utf8')
            f = io.StringIO(data)
            csv_import = Vereinstisch()
            csv_import.do_import(f)
            import_logs = csv_import.logs
            warnings = csv_import.warnings
    else:
        form = form_class()
    return render(request, 'erfaabgleich_import.html', {'form': form,
                                                        'logs': import_logs,
                                                        'warnings': warnings,
                                                        'h1': 'Vereinstisch Import'})


def vereinstisch_erfaliste(request):
    if request.method == "POST":
        exporter = ErfaCSVExporter(Erfa.objects.all())
        exporter.export()
        response = HttpResponse(exporter.file.getvalue(), content_type='application/text')
        response['Content-Disposition'] = 'attachment; filename=erfaliste.csv'
        return response
    else:
        form = ''
    return render(request, 'erfaabgleich_export.html', {'form': form, 'h1': 'Vereinstisch Erfaliste'})


def premiumadress_import(request):
    import_logs = None
    warnings = None
    form_class = SimpleFileUploadForm
    if request.method == 'POST':
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            input_string = request.FILES['docfile'].read().decode('iso-8859-15')
            import_logs, warnings = premiumaddress_reader(input_string)
    else:
        form = form_class()
    return render(request, 'erfaabgleich_import.html', {'form': form,
                                                        'logs': import_logs,
                                                        'warnings': warnings,
                                                        'h1': 'PremiumAdress Import'})


def show_transaction_log(request, pk=None):
    if pk:
        member = get_object_or_404(Member, chaos_number=pk)
        qs = BalanceTransactionLog.objects.filter(member=member).order_by('-created_on')
    else:
        qs = BalanceTransactionLog.objects.order_by('-created_on')
    paginator = Paginator(qs, 10)
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)

    page_query = qs.filter(id__in=[obj.id for obj in objects])
    context = {'objects': objects, 'transaction_logs': page_query}

    return render(request=request, context=context, template_name='show_transaction_logs.html')


def select_mail_reruns(request):
    qs = ArchivedEmail.objects.filter(email_address__startswith='sent as letter on').values('email_address')
    qs = qs.annotate(start_date=Min('sent_date'), description=Min('email_address'))
    qs = qs.order_by('-start_date')

    paginator = Paginator(qs, 20)
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)

    context = {'mail_runs': objects}

    return render(request=request, context=context, template_name='select_mail_reruns.html')


def do_mail_rerun(request, run_date):
    qs = ArchivedEmail.objects.filter(email_address__startswith=f'sent as letter on {run_date}')
    for mail in qs:
        try:
            member = Member.objects.get(chaos_number=mail.member_id)
            EmailToMember(email_type=mail.email_type, member=member).save()
        except Exception:
            pass

    return JsonResponse({'count': qs.count()})


def member_address_unknown(request):
    return render(request, 'address_unknown.html')


def member_email_unknown(request):
    return render(request, 'email_unknown.html')


def member_bulk_exit(request):
    return render(request, 'bulk_exit.html')


def anti_transaction(request, pk):
    if request.method == "POST":
        form = BalanceTransactionLogForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            d_initial = BalanceTransactionLog.objects.get(pk=pk)
            v = d_initial.changed_value * -1
            d_initial.member.increase_balance_by(v, comment="Reverted from:{} (was BalanceLog: {})".format(
                d_initial.comment, pk))
            # Unlink associated bank transaction and make it available for re-assignment
            if d_initial.bank_transaction:
                d_initial.bank_transaction.status = Transaction.STATUS_UNKNOWN_CHAOS_NR
                d_initial.bank_transaction.save()
                d_initial.bank_transaction = None
            return redirect('members:show_transaction_log')  # Redirect after POST
    else:
        inst = BalanceTransactionLog.objects.get(pk=pk)
        form = BalanceTransactionLogForm(instance=inst)

        form.fields['member'].widget = forms.HiddenInput()
        form.fields['changed_value'].widget = forms.HiddenInput()
        form.fields['new_value'].widget = forms.HiddenInput()
        form.fields['comment'].widget = forms.HiddenInput()

    return render(request, 'confirm_revert.html', {
        'form': form,
    })


def datenschleuder_address_stickers_selector(request):
    if request.method == 'POST':
        form = DSAddressLabelForm(request.POST)
        if form.is_valid():
            pa_id = form.cleaned_data['pa_id']
            num_pages = int(request.POST.get('num_pages'))
            pickup_date = datetime.strptime(request.POST.get('pickup_date'), '%Y-%m-%d').date()
            return datenschleuder_address_stickers(None, pa_label=pa_id, num_pages=num_pages, pickup_date=pickup_date)
    else:
        form = DSAddressLabelForm()
    return render(request, 'ds_sticker_select.html', {'form': form, 'ds_issue': settings.NEXT_DATENSCHLEUDER_ISSUE})


def ga_invitations_pa_selector(request):
    if request.method == 'POST':
        form = AddressLabelForm(request.POST)
        if form.is_valid():
            pa_label = form.cleaned_data['pa_label']
            return ga_invitations(pa_label=pa_label)
    else:
        form = AddressLabelForm()
    return render(request, 'pa_select.html', {'form': form, 'title': 'Generate General Assembly invitations'})


def bounces_import(request):
    """
    Mass removal of e-mail addresses which had bounce messages.
    :param request: Should contain an uploaded CSV file with the format: "timestamp", "ignored", "message id",
    "rest ignored"
    :return: A JSON object with a 'success' and an 'error' list of removed e-mail addresses
    """
    import_logs = None
    warnings = None
    form_class = SimpleFileUploadForm

    if request.method == 'POST':
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            uploaded_file = request.FILES['docfile']
            decoded_file = uploaded_file.read().decode('utf8')
            f = io.StringIO(decoded_file)
            reader = csv.DictReader(f, fieldnames=['timestamp', 'ignored', 'id'], delimiter=',', quotechar="\"")

            # Get the archived mails associated with the message numbers
            message_numbers = [line['id'] for line in reader]
            archived_mails = ArchivedEmail.objects.filter(message_number__in=message_numbers)

            # Delete the associated email addresses
            email_addresses = [archived_mail.email_address for archived_mail in archived_mails]
            EmailAddress.objects.filter(email_address__in=email_addresses).delete()

            # Resend those messages if possible
            for archived_mail in archived_mails:
                if archived_mail.email_type in EmailToMember.EMAIL_TO_SEND_TYPES and archived_mail.member:
                    EmailToMember(email_type=archived_mail.email_type, member=archived_mail.member).save()

    else:
        form = form_class()
    return render(request, 'erfaabgleich_import.html', {'form': form,
                                                        'logs': import_logs,
                                                        'warnings': warnings,
                                                        'h1': 'VERP Bounces Import'})


def check_teilnahmeschein(request):
    """
    Check if the member of a Teilnahmeschein is allowed to abstimm.
    :param request: Should contain a abstimmungs_token
    :return: A HTML page showing the status of the member
    """
    warning = False
    counts = {
        'scanned': AbstimmungToken.objects.all().exclude(status=AbstimmungToken.UNCOUNTED).count(),
        'valid': AbstimmungToken.objects.all().filter(status=AbstimmungToken.COUNTED).count(),
        'invalid': AbstimmungToken.objects.all().exclude(status=AbstimmungToken.COUNTED).exclude(
            status=AbstimmungToken.UNCOUNTED).count(),
    }
    counts['valid_percentage'] = counts['valid'] / counts['scanned']
    counts['invalid_percentage'] = counts['invalid'] / counts['scanned']

    if request.method == 'POST':
        form = TeilnahmeValidationForm(request.POST)
        if form.is_valid():
            teilnahme_token_or_chaos_number = form.cleaned_data['teilnahme_token']
            try:
                member = AbstimmungToken.objects.get(token=teilnahme_token_or_chaos_number.lower()).member
            except (AbstimmungToken.DoesNotExist, AbstimmungToken.MultipleObjectsReturned):
                try:
                    member = Member.objects.get(chaos_number=teilnahme_token_or_chaos_number)
                except (Member.DoesNotExist, Member.MultipleObjectsReturned, ValueError):
                    form.add_error('teilnahme_token', 'Kein Mitglied passend zu diesen Daten gefunden')
                    return render(request, 'validate_teilnahmeschein.html',
                                  {'form': form, 'warning': warning, 'counts': counts})

            # New empty form, so that the fields are emptied, but the error messages get added as feedback for the
            # previous entry
            emptied_post_request = request.POST.copy()
            emptied_post_request['teilnahme_token'] = None
            new_form = TeilnahmeValidationForm(emptied_post_request)

            if member.abstimmungtoken.status in [AbstimmungToken.EXITED, AbstimmungToken.NO_RIGHTS,
                                                 AbstimmungToken.COUNTED]:
                new_form.add_error(None, 'Dieser Abstimmungstoken wurde bereits gezählt!')
            else:
                member.abstimmungtoken.status = AbstimmungToken.COUNTED
                member.abstimmungtoken.date_of_count = date.today()

                if member.get_fees_payable_until_2023_05_21() != '0.00':
                    member.abstimmungtoken.status = AbstimmungToken.NOT_PAID
                    new_form.add_error(None, 'Mitgliedsbeiträge sind nicht beglichen')
                    warning = True

                if not member.is_active:
                    member.abstimmungtoken.status = AbstimmungToken.INACTIVE
                    new_form.add_error(None, 'Mitgliedschaft ruht')
                    warning = True

                if member.membership_type == Member.MEMBERSHIP_TYPE_SUPPORTER:
                    member.abstimmungtoken.status = AbstimmungToken.NO_RIGHTS
                    new_form.add_error(None, 'Fördermitglieder sind nicht stimmberechtigt')
                    warning = False

                if member.membership_end is not None:
                    if member.membership_end < date(2021, 4, 24):
                        member.abstimmungtoken.status = AbstimmungToken.EXITED
                        new_form.add_error(None, 'Mitglied ist ausgetreten')
                        warning = False

                member.abstimmungtoken.save()

            return render(request, 'validate_teilnahmeschein.html',
                          {'form': new_form, 'warning': warning, 'counts': counts})
    else:
        form = TeilnahmeValidationForm()
    return render(request, 'validate_teilnahmeschein.html', {'form': form, 'warning': warning, 'counts': counts})
