from django.test import TestCase
import gpg


class GpgTestCase(TestCase):

    def test_encryption_decryption(self):
        with gpg.Context(armor=True) as c:
            test_message = 'tästè'

            c.set_engine_info(gpg.constants.protocol.OpenPGP, home_dir='./gpg_home_folder')

            recipient_key = c.get_key('4B6B9174638813A83E8368ED059ACC5D37B1245E')
            encrypted_body, _, _ = c.encrypt(test_message.encode(), recipients=[recipient_key],
                                             always_trust=True, sign=False)

            result = c.decrypt(encrypted_body)
            returned_text = result[0].decode('utf-8')

            self.assertEqual(test_message, returned_text)

    def test_signature(self):
        with gpg.Context(armor=True) as c:
            test_message = 'tästè'

            c.set_engine_info(gpg.constants.protocol.OpenPGP, home_dir='./gpg_home_folder')
            sender_key = c.get_key('4B6B9174638813A83E8368ED059ACC5D37B1245E', secret=True)
            c.signers = [sender_key]

            signed_msg = c.sign(test_message.encode())

            result = c.verify(signed_msg[0])

            returned_text = result[0].decode('utf-8')

            self.assertEqual(test_message, returned_text)
