from django.urls import path, re_path

from . import views

app_name = 'members'

urlpatterns = [
    re_path(r'^import_members/', views.run_import_members, name='import_members'),
    re_path(r'^add_member/', views.add_member, name='add_member'),
    re_path(r'^search_form/', views.search_member_db, name='search_form'),
    re_path(r'^member_address_unknown/', views.member_address_unknown, name='memberAddressUnknown'),
    re_path(r'^member_email_unknown/', views.bounces_import, name='memberEmailUnknown'),
    re_path(r'^member_bulk_exit/', views.member_bulk_exit, name='memberBulkExit'),

    re_path(r'^statistics/monthly/', views.monthly_statistics, name='monthly_statistics'),
    re_path(r'^statistics/overview/', views.general_statistics, name='general_statistics'),
    re_path(r'^statistics/zip/', views.zip_statistics, name='zip_statistics'),

    re_path(r'^erfaabgleich/cashpoint_export/', views.cashpoint_export, name='cashpoint_export'),
    re_path(r'^erfaabgleich/vereinstisch_import/', views.vereinstisch_import, name='vereinstisch_import'),
    re_path(r'^erfaabgleich/vereinstisch_export/', views.vereinstisch_export, name='vereinstisch_export'),
    re_path(r'^erfaabgleich/vereinstisch_erfaliste/', views.vereinstisch_erfaliste, name='vereinstisch_erfaliste'),

    re_path(r'^openslides_export/', views.openslides_export, name='openslides_export'),
    re_path(r'^check_teilnahmeschein/', views.check_teilnahmeschein, name='check_teilnahmeschein'),

    re_path(r'^premiumadress_import/', views.premiumadress_import, name='premiumadress_import'),
    path('show_transaction_log/', views.show_transaction_log, name='show_transaction_log'),
    re_path(r'^show_transaction_log/(\d+)$', views.show_transaction_log, name='show_member_transaction_log'),
    path('select_mail_reruns/', views.select_mail_reruns, name='select_mail_reruns'),
    path('do_mail_rerun/<path:run_date>', views.do_mail_rerun, name='do_mail_rerun'),
    re_path(r'^anti_transaction/(\d+)$', views.anti_transaction, name='anti_transaction'),
    path('datenschleuder_address_stickers_selector/', views.datenschleuder_address_stickers_selector,
        name='datenschleuder_address_stickers_selector'),
    path('ga_invitations/', views.ga_invitations_pa_selector,
        name='ga_invitations_pa_selector'),
    re_path(r'^change_membership_type/(\d+)$', views.change_membership_type, name='change_membership_type'),
]
