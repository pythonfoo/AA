from collections import defaultdict

from django.urls import reverse
from django.test import Client
from hypothesis.extra.django import TestCase, from_model
from hypothesis.strategies import integers, characters, dates, sampled_from, composite, just, one_of
from hypothesis_csv.strategies import csv
from csv import Dialect, QUOTE_MINIMAL
from hypothesis import given, settings, assume, note

from members.models import DeliveryNumber, Person
from members.api_helper import post_countries
from django.core.files.uploadedfile import SimpleUploadedFile

c = Client()

delivery_num_strategy = from_model(DeliveryNumber,
                                   recipient=from_model(Person,
                                                        chaos_number=integers(min_value=1, max_value=100000000)
                                                        )
                                   )


class PostCSV(Dialect):
    delimiter = ';'
    quoting = QUOTE_MINIMAL
    quotechar = '"'
    lineterminator = '\r\n'
    doublequote = True
    escapechar = None


@composite
def csv_delivery_num(draw):
    delivery_num = draw(delivery_num_strategy)
    # iso8895-15 is roughly the Unicode subset from 0 to 0xFF
    # The differences are caught by catching encoding exceptions
    iso8895_15_compat = characters(min_codepoint=0, max_codepoint=0xFF, blacklist_characters='\0')
    sdgs = draw(one_of(integers(), sampled_from([10, 20, 30, 40])))
    adrmerk = draw(
        one_of(integers(), sampled_from([10, 11, 13, 14, 18, 19, 21, 22, 25, 30, 31, 50, 53, 55, 12, 20, 51])))
    delivery_num_number = draw(one_of(just(delivery_num.number), iso8895_15_compat))
    nsa = {
        'NSA_Na1': draw(iso8895_15_compat),
        'NSA_Na2': draw(iso8895_15_compat),
        'NSA_Na3': draw(iso8895_15_compat),
        'NSA_Na4': draw(iso8895_15_compat),
        'NSA_Str': draw(iso8895_15_compat),
        'NSA_HNr': draw(iso8895_15_compat),
        'NSA_PLZ': draw(iso8895_15_compat),
        'NSA_Ort': draw(iso8895_15_compat),
        'NSA_Land': draw(one_of(sampled_from(sorted(post_countries.keys())), iso8895_15_compat)),
        'NSA_Postf': draw(iso8895_15_compat),
        'NSA_PLZPostf': draw(iso8895_15_compat),
        'NSA_OrtPostfach': draw(iso8895_15_compat),
        'NSA_LandPostfach': draw(one_of(sampled_from(sorted(post_countries.keys())), iso8895_15_compat)),
    }
    csv_values = {
        'UebgID': integers(),
        'Fehlercodes': integers(),
        'PreAdrID': integers(),
        'ProdVar': integers(),
        'VersProd': integers(),
        'ProdID': integers(),
        'FrkArt': integers(),
        'FrkDat': dates().map(lambda d: d.strftime('%Y-%m-%d')),
        'EinlAbrNr': integers(),
        'SerienNr': integers(),
        'AuftrNr': integers(),
        'ZKZ': integers(),
        'HeftNr': integers(),
        'AboNr': integers(),
        'SdgNr': integers(),
        'Teiln': integers(),
        'KdInfoDMC': integers(),
        'SdgS': just(sdgs),
        'AdrMerk': just(adrmerk),
        'AbrProdNrAdress': integers(),
        'AbrProdRetoure': integers(),
        'AbrKontrakt': integers(),
        'Kd_Na1': iso8895_15_compat,
        'Kd_Na2': iso8895_15_compat,
        'Kd_Na3': iso8895_15_compat,
        'Kd_Na4': iso8895_15_compat,
        'Kd_Str': iso8895_15_compat,
        'Kd_HNr': iso8895_15_compat,
        'Kd_PLZ': iso8895_15_compat,
        'Kd_Ort': iso8895_15_compat,
        'Kd_Postf': iso8895_15_compat,
        'Kd_Info': just(delivery_num_number),
        'E_Na1': iso8895_15_compat,
        'E_Na2': iso8895_15_compat,
        'E_Na3': iso8895_15_compat,
        'E_Na4': iso8895_15_compat,
        'E_Str': iso8895_15_compat,
        'E_HNr': iso8895_15_compat,
        'E_PLZ': iso8895_15_compat,
        'E_Ort': iso8895_15_compat,
        'E_Postf': iso8895_15_compat,
        'NSA_Na1': just(nsa['NSA_Na1']),
        'NSA_Na2': just(nsa['NSA_Na2']),
        'NSA_Na3': just(nsa['NSA_Na3']),
        'NSA_Na4': just(nsa['NSA_Na4']),
        'NSA_Str': just(nsa['NSA_Str']),
        'NSA_HNr': just(nsa['NSA_HNr']),
        'NSA_PLZ': just(nsa['NSA_PLZ']),
        'NSA_Ort': just(nsa['NSA_Ort']),
        'NSA_Land': just(nsa['NSA_Land']),
        'NSA_Postf': just(nsa['NSA_Postf']),
        'NSA_PLZPostf': just(nsa['NSA_PLZPostf']),
        'NSA_OrtPostfach': just(nsa['NSA_OrtPostfach']),
        'NSA_LandPostfach': just(nsa['NSA_LandPostfach']),
        'KdInfoDMC_Dezimal': integers(),
        'KdInfoDMC_Ascii': iso8895_15_compat,
    }
    csv_draw = draw(csv(header=csv_values.keys(), columns=csv_values.values(), lines=1, dialect=PostCSV))
    return csv_draw, delivery_num, delivery_num_number, sdgs, adrmerk, nsa


def addr_unk(person_orig, person_mod, delivery_number, _):
    """No failure and the address unknown counter should be counted up"""
    assert person_orig.address_unknown + 1 == person_mod.address_unknown
    assert delivery_number.returned is True


def new_addr(person_orig, person_mod, _, nsa):
    """The address should be changed and address_unknown should be 0"""
    is_postfach = bool(nsa['NSA_Postf'])

    country = nsa['NSA_LandPostfach'] if is_postfach else nsa['NSA_Land']
    if country in post_countries:
        assert (post_countries[country] == person_mod.address_country)
    else:
        failed(person_orig, person_mod, None, nsa)
        return

    def rm_whitespace(string):
        return ''.join(string.split())

    address_from_person = rm_whitespace(person_mod.first_name + person_mod.last_name + (person_mod.address_1 or '') + (
            person_mod.address_2 or '') + (person_mod.address_3 or ''))

    if is_postfach:
        assert (address_from_person == rm_whitespace(nsa['NSA_Na1'] + nsa['NSA_Na2'] + nsa['NSA_Na3'] + nsa['NSA_Na4'] +
                nsa['NSA_Postf'] + nsa['NSA_PLZPostf'].zfill(5) + nsa['NSA_OrtPostfach']))
    else:
        assert (address_from_person == rm_whitespace(nsa['NSA_Na1'] + nsa['NSA_Na2'] + nsa['NSA_Na3'] + nsa['NSA_Na4'] +
                nsa['NSA_Str'] + nsa['NSA_HNr'] + nsa['NSA_PLZ'].zfill(5) + nsa['NSA_Ort']))
    assert (person_mod.address_unknown == 0)


def death(_1, person_mod, _2, _3):
    """Person has been removed from the database"""
    assert (person_mod is None)


def failed(person_orig, person_mod, _1, _2):
    """No changes in person object"""
    assert (person_orig == person_mod)


def failed_row():
    return defaultdict(ret_failed)


def ret_failed():
    return failed


"""This table represents all possible combinations and their desired outcomes for Sendungsschicksal and Adressvermerk.
Usage: state_table[AdrMerk][SdgS] -> test_function(person, ...)"""
state_table = defaultdict(failed_row, {
    10: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    11: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    12: defaultdict(ret_failed, {10: death, 20: failed, 30: failed, 40: death}),
    13: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    14: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    18: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    19: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    20: defaultdict(ret_failed, {10: new_addr, 20: new_addr, 30: failed, 40: new_addr}),
    21: defaultdict(ret_failed, {10: addr_unk, 20: addr_unk, 30: failed, 40: addr_unk}),
    22: defaultdict(ret_failed, {10: addr_unk, 20: addr_unk, 30: failed, 40: addr_unk}),
    25: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    30: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    31: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    50: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    51: defaultdict(ret_failed, {10: new_addr, 20: new_addr, 30: failed, 40: new_addr}),
    53: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
    55: defaultdict(ret_failed, {10: addr_unk, 20: failed, 30: failed, 40: addr_unk}),
})


class ImportPremiumAdressTestCase(TestCase):

    @given(csv_delivery_num())
    @settings(deadline=None, max_examples=1000)
    def test_import_premium_adress(self, tupled_test_data):
        csv_string, delivery_num, delivery_num_number, sdgs, adrmerk, nsa = tupled_test_data
        try:
            encoded = csv_string.encode("iso-8859-15")
        except UnicodeEncodeError:
            assume(False)
            return

        # Take note of person state before import
        person_orig = delivery_num.recipient

        csv_upload = SimpleUploadedFile("upload.csv", encoded, content_type="text/plain")
        response = c.post(reverse('members:premiumadress_import'), {'docfile': csv_upload})

        # No matter what error happens, the webpage should load properly
        assert response.status_code == 200

        person_mod = Person.objects.filter(chaos_number=person_orig.chaos_number).first()

        # Special case of wrong delivery number
        if delivery_num_number != delivery_num.number:
            failed(person_orig, person_mod, None, None)
            return

        # Check for all different kinds of possible combinations
        action = state_table[adrmerk][sdgs]
        note('Sdgs: {} AdrMerk: {} Action: {}'.format(sdgs, adrmerk, action.__name__))
        delivery_num_mod = DeliveryNumber.objects.filter(number=delivery_num.number).first()
        action(person_orig, person_mod, delivery_num_mod, nsa)
