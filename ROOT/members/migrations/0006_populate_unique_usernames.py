# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from members.models import _unique_username


def gen_unique_usernames(apps, schema_editor):
    Member = apps.get_model('members', 'Member')
    for mbr in Member.objects.all():
        mbr.username = _unique_username()
        mbr.save(update_fields=['username'])


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0005_auto_20210212_1810'),
    ]

    operations = [
        migrations.RunPython(gen_unique_usernames, reverse_code=migrations.RunPython.noop),
    ]
