from django.urls import path, re_path

from . import views
from django.db.models import Q
from .models import Transaction

app_name = 'import_app'

urlpatterns = [
    re_path(r'^import_banking_csv/', views.run_import_banking, name='import_banking'),
    path('member_autocomplete/', views.MemberAutocomplete.as_view(), name='member_autocomplete'),
    re_path(r'^manage_transactions/', views.manage_transactions,
        {'queryset': Transaction.objects.filter(
            Q(status=Transaction.STATUS_UNKNOWN_CHAOS_NR) | Q(status=Transaction.STATUS_MATCHED_CHAOS_NR)).order_by(
            'booking_day', 'id')},
        name='manage_transactions'),
    re_path(r'^manage_transactions_failed/', views.manage_transactions,
        {'queryset': Transaction.objects.filter(status=Transaction.STATUS_FAILED).order_by('booking_day', 'id')},
        name='manage_transactions_failed'),
    re_path(r'^manage_transactions_ignored/', views.manage_transactions,
        {'queryset': Transaction.objects.filter(status=Transaction.STATUS_IGNOREFOREVER).order_by('-booking_day', '-id')},
        name='manage_transactions_ignored'),
]
