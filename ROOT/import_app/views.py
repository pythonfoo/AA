import csv
import io
import logging

import import_app.csv_parser as csv_parser
from django.shortcuts import render
from django.db.models import Q
from import_app.forms_import import BankingImportForm
from members.models import Member, EmailToMember
from dal import autocomplete

from .models import DuplicateBankingWarning, NegativeCreditError, Transaction
from .forms import TransactionFormSet

logging.getLogger(__name__)
logging.getLogger().setLevel(logging.INFO)


def run_import_banking(request):
    # Handle file upload
    uploaded_file = ''
    errors = []
    imported = []
    context = {}
    if request.method == 'POST':
        form = BankingImportForm(request.POST, request.FILES)
        if form.is_valid():
            uploaded_file = request.FILES['docfile']
            data = uploaded_file.read().decode("utf8")
            f = io.StringIO(data)
            reader = csv.reader(f, delimiter=';')
            # The first 8 rows are headers
            for i in range(8):
                next(reader)
            duplicate_warnings = []
            for row in reader:
                logging.debug("reading row: {}".format(row))
                if row[0] == "Kontostand":
                    break
                new_transaction = csv_parser.TransactionReader(row)
                new_tr_entry = Transaction()
                try:
                    new_tr_entry.add_transaction(new_transaction)
                except DuplicateBankingWarning:
                    duplicate_warnings.append('Duplicated entry not added: {}'.format(new_tr_entry.string_in))
                except NegativeCreditError:
                    logging.info("Outgoing transaction skipped")
            if len(duplicate_warnings) > 0:
                context["warnings"] = duplicate_warnings
            context["message"] = "uploaded successfully"

    form = BankingImportForm()  # An empty, unbound form

    last_transaction = Transaction.objects.all().order_by('booking_day').last()
    last_transaction_date = last_transaction.booking_day if last_transaction else None

    context.update({'form': form, 'uploadedFile': uploaded_file, 'errors': errors, 'imported': imported,
                    'latest_import': last_transaction_date})
    logging.info("context is {}".format(context))
    return render(request, 'import_banking_csv.html', context)


class MemberAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Member.objects.none()

        qs = Member.objects.all()

        if self.q:
            qs = qs.filter(
                Q(chaos_number__icontains=self.q) | Q(first_name__icontains=self.q) | Q(
                    last_name__icontains=self.q) | Q(transfer_token__trigram_similar=self.q.upper()))

        return qs

    def get_result_label(self, item):
        """How the member autocomplete suggestions are displayed"""
        return "{}: {} {} ({})".format(item.chaos_number, item.first_name, item.last_name,
                                       "active" if item.is_active else "inactive")


def manage_transactions(request, queryset=Transaction.objects.all()):
    if request.method == 'POST':
        formset = TransactionFormSet(data=request.POST, files=request.FILES, queryset=queryset,
                                     page_num=request.GET.get('page'))
        if formset.is_valid():
            for form in formset:
                if form.instance.status == Transaction.STATUS_MATCHED_CHAOS_NR and form.instance.member:
                    form.instance.book()
                    EmailToMember(email_type=EmailToMember.SEND_TYPE_HAND_MATCHED,
                                  member=Member.objects.get(chaos_number=form.instance.member.chaos_number)).save()
                else:
                    form.instance.save()
        else:
            return render(request, 'manage_transactions_autocomplete.html',
                          {'formset': formset, 'status_matching': Transaction.STATUS_MATCHED_CHAOS_NR})

    formset = TransactionFormSet(queryset=queryset, per_page=10, page_num=request.GET.get('page'))
    return render(request, 'manage_transactions_autocomplete.html',
                  {'formset': formset, 'status_matching': Transaction.STATUS_MATCHED_CHAOS_NR})
