from django.contrib import admin
from import_app.models import Transaction


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('member', 'rating', 'status', 'information')


