from dal import autocomplete

from django import forms
from .models import Transaction
from members.models import Member
from paginated_modelformset import PaginatedModelFormSet


class MemberModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, item):
        """How the member object is displayed in the field's box once it's selected"""
        return "{}: {} {} ({})".format(item.chaos_number, item.first_name, item.last_name,
                                       "active" if item.is_active else "inactive")


class TransactionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TransactionForm, self).__init__(*args, **kwargs)
        self.fields['information'].disabled = True
        self.fields['information'].required = False
        self.fields['payer'].disabled = True
        self.fields['amount'].disabled = True

    member = MemberModelChoiceField(
        queryset=Member.objects.all(),
        widget=autocomplete.ModelSelect2(url='import_app:member_autocomplete'),
        required=False
    )

    class Meta:
        model = Transaction
        fields = ['information', 'member', 'payer', 'amount', 'status']
        readonly = ['information', 'payer', 'amount']


TransactionFormSet = forms.modelformset_factory(Transaction, extra=0, form=TransactionForm,
                                                formset=PaginatedModelFormSet)
