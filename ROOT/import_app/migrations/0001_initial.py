# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('string_in', models.CharField(unique=True, max_length=1000)),
                ('booking_day', models.DateField(null=True)),
                ('available_on', models.DateField(null=True)),
                ('payment_type', models.CharField(max_length=80)),
                ('information', models.TextField()),
                ('referenz', models.TextField()),
                ('verwendungszweck', models.TextField()),
                ('payer', models.CharField(max_length=80)),
                ('payee', models.CharField(max_length=80)),
                ('amount', models.IntegerField()),
                ('balance', models.IntegerField()),
                ('chaos_number', models.IntegerField()),
                ('chaosnr_options', models.CharField(null=True, max_length=200, blank=True)),
                ('rating', models.PositiveSmallIntegerField()),
                ('status', models.CharField(max_length=1, choices=[('M', 'matched chaos nr'), ('U', 'unknown chaos nr'), ('C', 'completed'), ('I', 'ignore forever'), ('F', 'failed at booking')])),
            ],
        ),
    ]
