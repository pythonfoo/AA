from django.urls import path, re_path

from . import views

app_name = 'api'

urlpatterns = [
    re_path(r'^mail_send_next/', views.mail_send_next, name='mail_send_next'),
    re_path(r'^mail_send_all/', views.mail_send_all, name='mail_send_all'),
    re_path(r'^pgp_test/', views.pgp_test, name='pgp_test'),
    re_path(r'^msg_stats/', views.msg_stats, name='msgs_still_to_send'),
    re_path(r'^generate_letters/', views.generate_letters_pa_selector, name='generate_letters'),
    re_path(r'^billing_cycle/', views.billing_cycle, name='billing_cycle'),
    re_path(r'^zip_analysis/', views.zip_analysis, name='zip_analysis'),
    re_path(r'^country_analysis/', views.country_analysis, name='country_analysis'),
    re_path(r'^search_member_db/', views.search_member_db, name='search_member_db'),
    re_path(r'^get_erfa_statistics/', views.get_erfa_statistics, name='get_erfa_statistics'),
    re_path(r'^drop_transactions/', views.drop_transactions, name='drop_transactions'),
    re_path(r'^address_unknown/', views.bulk_address_unknown, name='api_bulk_address_unknown'),
    re_path(r'^bulk_exit/', views.bulk_remove_members, name='api_bulk_exit'),
    re_path(r'^send_data_record/', views.member_queue_data_record_email, name='send_data_record'),
    re_path(r'^member_reactivate/', views.member_reactivate, name='member_reactivate'),
    path('reactivation_reminder/', views.mass_reactivation_reminder, name='reactivation_reminder'),
    path('inactive_members/', views.inactive_members, name='inactive_members'),
    path('no_contact/', views.no_contact, name='no_contact'),
    path('ga_invitations_email/', views.ga_emails, name='ga_invitations_email'),
    path('datenschleuder_address_stickers/<int:pa_id>', views.datenschleuder_address_stickers,
        name='datenschleuder_address_stickers'),
    path('datenschleuder_address_csv/', views.datenschleuder_address_csv_export,
        name='datenschleuder_address_csv'),
]
